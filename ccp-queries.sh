#!/bin/bash

set -x

# CCP Query - Retrieve by Object
#################################

curl -s --cacert comp01.pem --url "https://comp01.cybr.com/AIMWebService/api/Accounts?AppID=workshop_test_appid&Query=Safe=CYBR_AAM_WORKSHOP;Object=Operating%20System-WinServerLocal-workshop_test_address-workshop_test_user" | jq . 

# CCP Query - Retrieve by Username
#############################################

curl -s --cacert comp01.pem --url "https://comp01.cybr.com/AIMWebService/api/Accounts?AppID=workshop_test_appid&UserName=workshop_test_user" | jq .Content
