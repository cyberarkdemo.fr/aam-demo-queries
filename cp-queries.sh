#!/bin/bash

set -x

# CP Query - Retrieve by object
#################################

/opt/CARKaim/sdk/clipasswordsdk GetPassword -p AppDescs.AppID=workshop_test_appid -p Query="Safe=CYBR_AAM_WORKSHOP;Folder=Root;Object=Operating System-WinServerLocal-workshop_test_address-workshop_test_user" -p RequiredProps=UserName,Address -o PassProps.Username,PassProps.Address,Password


# CP Query - Retrieve by (Username,Address)
#############################################

/opt/CARKaim/sdk/clipasswordsdk GetPassword -p AppDescs.AppID=workshop_test_appid -p Query="Safe=CYBR_AAM_WORKSHOP;Username=workshop_test_user;Address=workshop_test_address" -o Password


